var userModel = require("./../model/userModel");

var mongoose = require("mongoose");
require("./commonController");

getUserDetail = function(req, res){
    var user_id = req.user._id;
    var search_user = {_id:user_id};
    userModel.getUser(search_user, function(err, data){
        var return_val = {};
        if (err){
            return_val.success = false;
			return_val.error = err;
            res.json(return_val);
        }
        else{
            if (data == null) {
				return_val.success = false;
                return_val.error = "User doesn't exist.";
                res.json(return_val);
            }
            else{
                return_val.success = true;
				return_val.data = data;
                res.json(return_val);
            }
        }  
    });  
}

updateUserDetail = function(req, res){

    //Code to create JSON object data
    var update_data = {};
  /*  if(typeof req.body.username != "undefined"){
        update_data.username = req.body.username;
    } */
    if(typeof req.body.full_name != "undefined"){
        update_data.full_name = req.body.full_name;
    }

    if(typeof req.body.is_deleted != "undefined"){
        update_data.is_deleted = req.body.is_deleted;
    }
    if(typeof req.body.is_active != "undefined"){
        update_data.is_active = req.body.is_active;
    }
    update_data.modified = Date.now();
    //End of code to create object data
    
    // Code to update clinic Details
    var search_criteria = {_id:req.user._id};

    userModel.updateUser(search_criteria, update_data, function(err, data){
        var return_data = {};
        var message = "";
        if (err) {
            if (err.errors) {
                var error_detail = [];
                //var error = [];
                for (var errName in err.errors) {
                    error_detail.push(err.errors[errName].message);
                }
				return_data.success =false;
                return_data.error = error_detail;
                res.json(return_data);
            }
            else{
				return_data.success =false;
                return_data.error = err;
                res.json(return_data);
            }
        }
        else{
            res.json({"success":true}); 
        }
    });
    //End of code to update clinic detail
}



module.exports = function(){
    this.getUserDetail = getUserDetail;
    this.updateUserDetail = updateUserDetail;
}



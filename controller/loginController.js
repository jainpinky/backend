var userModel = require('./../model/userModel');
var config = require('./../config');
var jwt = require('jsonwebtoken');

userlogin = function(req, res, next){
	var obj = {};
	obj.email = req.body.email;
	var rememberMe = req.body.rememberMe;
	//obj.password = req.body.password;
	if(typeof req.body.admin != "undefined"){
		obj.user_type = 0;
	}
	if(typeof req.body.email != "undefined" && typeof req.body.password != "undefined"){
		userModel.findOne(obj, function(err, data){
			if(err){
				res.json({"code":400, "success":false, msg:err});
			}
			else if(data == null){
				res.json({"code":400, "success":false, msg:"Invalid credentials"});	
			}
			else{
				var validpassword = userModel.validatePassword(req.body.password,data.enc_password);
				if(validpassword != true){
					res.json({"code":400, "success":false, msg:"Invalid credentials"});			
				}
				else{
					var userData = {};
					userData.last_login = Date.now();
					userModel.update({_id:data._id}, {$set:userData}, function(errUpd, dataUpd){
						var sessionValue = {};
						sessionValue._id = data._id;
						sessionValue.email = data.email;
						sessionValue.full_name = data.full_name;
						if(typeof data.profile_image == "undefined"){
							sessionValue.profile_image = data.profile_image;
						}
						sessionValue.user_type = data.user_type;
						if(typeof data.profile_image != "undefined"){
							sessionValue.profile_image = data.profile_image;
						}

						if(typeof rememberMe != "undefined" && rememberMe == true){
							sessionValue.rememberMe = true;
							var token = jwt.sign(sessionValue, config.jwtSecret);
						}
						else{
							sessionValue.rememberMe = false;
							var token = jwt.sign(sessionValue, config.jwtSecret);
						}
						sessionValue.token = token;
						res.json({success:true, detail:sessionValue});
					});
				}
			}
			
		});
	}
	else{
		res.json({"code":400, "success":false, msg:"Email and password is required"});	
	}
}

function randomString(length) {
    var result = '';
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

module.exports = function(){
	this.userlogin = userlogin;
}
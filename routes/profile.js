module.exports = function(app,express, auth){
    var router = express.Router();
    var profileController = require("./../controller/profileController");
    var profileObj = new profileController();
    
    router.get('/detail', auth.authenticate(), function(req, res, next){
		res.json({'success':true, 'detail':req.user});
	});
	
	app.use('/profile',router);
}


module.exports = function(app, express){
	var router = express.Router();
	var loginController = require("../controller/loginController");
	var loginObj = new loginController();
	
	// Login functionality
	router.post('/', function(req, res, next){
		loginObj.userlogin(req, res, next);
	});

	//logout functionality
	router.get('/logout', function(req, res, next){
		loginObj.userlogout(req, res, next);
	});
	
	app.use('/login', router);
}
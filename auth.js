// auth.js
var passport = require("passport");  
var passportJWT = require("passport-jwt");  
var cfg = require("./config.js");  
var ExtractJwt = passportJWT.ExtractJwt;  
var Strategy = passportJWT.Strategy;  
var params = {  
    secretOrKey: cfg.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

module.exports = function() {  
    var strategy = new Strategy(params, function(payload, done) {
		return done(null, payload);
    });
    passport.use(strategy);
    return {
        initialize: function() {
            return passport.initialize();
        },
        authenticate: function(res) {
            //res.header({'access-token':"invalid"});
            return passport.authenticate("jwt", cfg.jwtSession);
        }
    };
};
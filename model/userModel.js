var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
var Schema = mongoose.Schema;

var userSchema = new Schema({
	full_name:{type:String},
	email:{type:String, required:"email name is required"},
	enc_password:{type:String},
	user_type:{type:Number, required:"User Type is required"}, //0= Super Admin, 1 = normal user 2 = fan
	last_login:{type:Number},
	is_deleted:{type:Number, default:0},
	is_active:{type:Number, default:1},
	created:{type:Number},
	modified:{type:Number}
});

var userModel = mongoose.model('User', userSchema);

userModel.getUser = function(userDetail, fn){
	userModel.findOne(userDetail, fn).lean();
}
userModel.listUser = function(search_creteria, fn){
	userModel.find(search_creteria, fn);
}
userModel.addUser = function(userDetail, fn){
	var add_user = new userModel(userDetail);
	add_user.save(fn);
}
userModel.updateUser = function(search_criteria, new_data, fn){
	userModel.update(search_criteria, {$set:new_data},  {runValidators:true}, fn);
}

// Generates hash using bCrypt
userModel.createHash = function(password){
 return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
}
// compare password
userModel.validatePassword = function(password, orgPassword) {	
	return bcrypt.compareSync(password, orgPassword);
};


module.exports = userModel;


// Validate Username

userModel.schema.path('email').validate(function (value, respond) {
	userModel.findOne({ "email": value }, function (err, user) {                                                                                                
        if(user){
			respond(false, "This email is already registered");
		}
		else{
			respond(true, "This email is already registered");
		}
    });
}, 'This email is already registered');



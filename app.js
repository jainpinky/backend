var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config');
var auth = require("./auth")();  
var jwt = require('jsonwebtoken');

var blacklist = require('express-jwt-blacklist');
var expressSession = require('express-session');

// code for Passport Login


//End of code for Passport Login

var app = express();
  // Database Connection
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/mean');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  console.log('Database connected');
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '20mb'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.set('superSecret', "MEANProject");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, Authorization, Content-Type, Accept");
  if ('OPTIONS' == req.method) {
    res.json({"success":true});
  }
  else {
    next();
  }
});
//End of code for Passport Session

app.use('/logout', function(req, res, next){
  res.json("logout");
});

require('./routes/login')(app, express);
require('./routes/users')(app, express, auth);
require('./routes/profile')(app, express, auth);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

//


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;

